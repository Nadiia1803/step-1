// Our Services
const tabs = document.querySelector('.menu-of-services');
const tabsContent = document.querySelector('.content-of-tabs');


console.log(tabs);
console.log(tabsContent);

function handlerEventListener(event) {
    tabs.querySelector('.active').classList.remove('active');
    event.target.classList.add('active');
    tabsContent.querySelector('li:not([hidden])').classList.remove('tabs-content');
    tabsContent.querySelector('li:not([hidden])').hidden = true;
    tabsContent.children[event.target.dataset.index].hidden = false;
    tabsContent.children[event.target.dataset.index].classList.add('tabs-content');
}

for (let i = 0; i < tabs.childElementCount; i++) {
    tabs.children[i].dataset.index = i;
    if (i) {
        tabsContent.children[i].hidden = true;
        tabsContent.children[i].classList.remove('tabs-content');
    }
}

tabs.addEventListener('click', handlerEventListener);


// Our Amazing Work

const workMenu = document.querySelector('.work-menu-list');

workMenu.addEventListener('click', workMenuEventListener);

const gallery2 = document.querySelector('.work-gallery');

// отримали колекцію
const allImg = Array.from(gallery2.children);
console.log(allImg);
allImg.forEach((item, index) => {
    console.log(item);
    console.log(index);
    if (index > 11) {
        item.classList.add('img-hidden');
    }

});

function workMenuEventListener(event) {
    // убираем текущий селектор и ставим его в выбраный элемент
    workMenu.querySelector('.active-work').classList.remove('active-work');
    event.target.classList.add('active-work');

    // вызываем функцию изменения видимости
    SetVisabilityByDataItem(allImg, event.target.getAttribute('data-item'));
}
// функция для сортировки массива картинок
// принимает значение массив и типа из выбраного поля аля 'graphic'
function SetVisabilityByDataItem(arr, type) {

    for (let i = 0; i < arr.length; i++) {
        // делаем все видимым
        arr[i].classList.remove('img-hidden');

        // или для всех где индекс больше 11(12 - элеентов)
        // ставим невидимоть если тим не все и не равно выбраноуму тиав из таргета
        if ((type === 'all' && i > 11) ||
            (type !== 'all' && arr[i].getAttribute('data-item') !== type)) {
            arr[i].classList.add('img-hidden');
        }
    }
}


// What People Say About theHam

$('.sl-1').slick({
    // autoplaySpeed:10000,
    // autoplay: true,
    cssEase: 'ease-in',
    asNavFor:'.sl-2',
     // arrows: false
});



$('.sl-2').slick({
    dots: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    centerMode: true,
    centerPadding: true,
    asNavFor: '.sl-1',
    focusOnSelect: true,
     // arrows: true
});

